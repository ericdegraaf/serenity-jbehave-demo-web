Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: scenario description
Meta:

Given I need to buy some groceries
When I add the todo action "Buy five apples"
And I add the todo action "Buy three pears"
And I add the todo action "Buy some bananas"
Then "Buy three pears" should be recorded in my todo list
When I delete all todo actions