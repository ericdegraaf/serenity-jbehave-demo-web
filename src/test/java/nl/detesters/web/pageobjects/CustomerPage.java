package nl.detesters.web.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

/**
 *
 */
@DefaultUrl("http://localhost/customer/index.html")
public class CustomerPage extends PageObject {
}
