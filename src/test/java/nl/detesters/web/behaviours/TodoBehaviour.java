package nl.detesters.web.behaviours;

import net.thucydides.core.annotations.Steps;
import nl.detesters.web.steps.TodoSteps;
import org.jbehave.core.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Stefan on 10/9/2016.
 */
public class TodoBehaviour {
    private final static Logger LOG = LoggerFactory.getLogger(TodoBehaviour.class);

    @Steps
    TodoSteps todoSteps;

    @Given("I need to $doSomething")
    public void givenTheTodoApplicationIsOpened() {
        todoSteps.opens_the_todo_application();
    }

    @When("I add the todo action \"$actionName\"")
    public void whenIAddTheTodoAction(@Named("actionName") final String actionName) {
        todoSteps.adds_an_action_called(actionName);
    }

    @When("I delete all todo actions")
    public void whenIDeleteAllActions() {
        todoSteps.delete_all_actions();
    }

    @Then("\"$action\" should appear in my todo list")
    @Alias("\"$action\" should be recorded in my todo list")
    public void action_should_appear_in_my_todo_list(String action) throws Throwable {
        todoSteps.should_see_the_todo_action(action);
    }
}
