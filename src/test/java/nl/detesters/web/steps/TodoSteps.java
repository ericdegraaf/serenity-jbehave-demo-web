package nl.detesters.web.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import nl.detesters.web.pageobjects.TodoPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by Stefan on 10/9/2016.
 */
public class TodoSteps extends ScenarioSteps {
    private final static Logger LOG = LoggerFactory.getLogger(TodoSteps.class);

    TodoPage todoPage;

    @Step
    public void opens_the_todo_application() {
        todoPage.open();
    }

    @Step
    public void adds_an_action_called(String actionName) {
        todoPage.addAnActionCalled(actionName);
    }

    @Step
    public void delete_all_actions() {
        todoPage.deleteAllActions();
    }

    @Step
    public void should_see_the_todo_action(String action) {
        assertThat("The todo list does not contain expected action!",
                todoPage.getActions(),
                hasItem(action)
        );
    }
}
